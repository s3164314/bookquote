package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {

    Map<String, Double> isbnMap;

    public Quoter() {
        isbnMap = new HashMap<>();
        isbnMap.put("1", 10.0);
        isbnMap.put("2", 45.0);
        isbnMap.put("3", 20.0);
        isbnMap.put("4", 35.0);
        isbnMap.put("5", 50.0);
    }
    public double getBookPrice(String isbn) {
        if (isbnMap.containsKey(isbn)) {
            return isbnMap.get(isbn);
        } else {
            return 0.0;
        }
    }
}